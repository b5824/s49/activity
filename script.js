/* 
    Fetch - will send a request to the server and then respond with a JSON file.
    
    Syntax:
        fetch("url", options)
            URL - this is the URL where the request will be sent to
            options - an array of properties, optional paramater
*/
//NOTE: Fetch
fetch('https://jsonplaceholder.typicode.com/posts').then((response) => {
  //Whatever response we get, will be converted to JSON
  response.json().then((data) => showPosts(data));
});

//NOTE: Show posts
const showPosts = (posts) => {
  let postEntries = '';

  posts.forEach((post) => {
    console.log(post);
    postEntries += `
    <div id="post-${post.id}">
        <h3 id="post-title-${post.id}">${post.title}</h3>
        <p id="post-body-${post.id}">${post.body}</p>
        <button onclick="editPost('${post.id}')">Edit</button>
        <button onclick="deletePost('${post.id}')">Delete</button>
    </div>
        `;
  });

  document.querySelector('#div-post-entries').innerHTML = postEntries;
};

//NOTE: Add Post
document.querySelector('#form-add-post').addEventListener('submit', (e) => {
  e.preventDefault();

  fetch('https://jsonplaceholder.typicode.com/posts', {
    method: 'POST',
    body: JSON.stringify({
      title: document.querySelector('#txt-title').value,
      body: document.querySelector('#txt-body').value,
      userId: 1,
    }),
    headers: {
      'Content-Type': 'application/json',
    },
  })
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      alert('Successfully Added');

      document.querySelector('#txt-edit-id').value = null;
      document.querySelector('#txt-title').value = null;
      document.querySelector('#txt-body').value = null;
    });
});

const editPost = (id) => {
  let title = document.querySelector(`#post-title-${id}`).innerHTML;
  let body = document.querySelector(`#post-body-${id}`).innerHTML;

  document.querySelector('#txt-edit-id').value = id;
  document.querySelector('#txt-edit-title').value = title;
  document.querySelector('#txt-edit-body').value = body;

  document.querySelector('#btn-submit-update').removeAttribute('disabled');
};

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
  e.preventDefault();

  fetch('https://jsonplaceholder.typicode.com/posts/1', {
    method: 'PUT',
    body: JSON.stringify({
      id: document.querySelector('#txt-edit-id').value,
      title: document.querySelector('#txt-edit-title').value,
      body: document.querySelector('#txt-edit-body').value,
      userId: 1,
    }),
    headers: {
      'Content-Type': 'application/json',
    },
  })
    .then((response) => response.json())
    .then((data) => {
      //   console.log(data);
      alert('Post successfully updated');

      document.querySelector('#txt-edit-id').value = null;
      document.querySelector('#txt-edit-title').value = null;
      document.querySelector('#txt-edit-body').value = null;

      document
        .querySelector('#btn-submit-update')
        .setAttribute('disabled', true);
    });
});

// Activity:
/*
    >> Write the necessary code to delete a post
    >> Make a function called deletePost.
    >> Pass a parameter id.
    >> Use fetch method and the options

*/
//! Activity
const deletePost = (id) => {
  fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
    },
  })
    .then((result) => result.json())
    .then((data) => {
      alert('Successfully deleted');
      document.querySelector(`#post-${id}`).remove();
    });
};

/* const deletePost = (id) => {
  fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
    method: 'DELETE',
    body: JSON.stringify({
      id: document.querySelector(`#post-${id}`).value,
      body: document.querySelector(`#post-body-${id}`).value,
      title: document.querySelector(`#post-title-${id}`).value,
      userId: `${id}`,
    }),
    headers: {
      'Content-Type': 'application/json',
    },
  })
    .then((result) => result.json())
    .then((data) => {
      alert('Successfully deleted');
      document.querySelector(`#post-${id}`).remove();
    });
}; 
 */

/* 
    Reading List:
    >>https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
*/
